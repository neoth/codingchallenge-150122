//
//  ShiftAPITests.swift
//  CodingChallengeTests
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import UIKit
import XCTest
import Combine
@testable import CodingChallenge

class ShiftAPITests: XCTestCase {
	override func setUpWithError() throws {
		// Put setup code here. This method is called before the invocation of each test method in the class.
	}

	override func tearDownWithError() throws {
		// Put teardown code here. This method is called after the invocation of each test method in the class.
	}

	func testFetchShifts() throws {
		let expectation = expectation(description: "Fetch list of shifts for the upcoming week in Dallas")
		
		var disposables: Set<AnyCancellable> = []
		let shiftsFetcher = ShiftsFetcher()
		let startDate = Date()
		let endDate = Calendar.current.date(byAdding: .weekOfYear, value: 1, to: startDate)
		let queryModel = AvailableShiftsQueryModel(type: .list,
												   address: "Dallas",
												   start: startDate.dateString,
												   end: endDate?.dateString,
												   radius: nil)
		shiftsFetcher.fetchAvailableShifts(queryModel: queryModel)?
			.sink(receiveCompletion: {
				switch $0 {
				case .failure(let error):
					print(error.localizedDescription)
				default:
					return
				}
			}) { response in
				if response.data.count > 0 { expectation.fulfill() }
			}
			.store(in: &disposables)
		
		wait(for: [expectation], timeout: 7)
	}
}
