//
//  ShiftsViewModelTests.swift
//  CodingChallengeTests
//
//  Created by Brady Miller on 4/7/21.
//

import XCTest
@testable import CodingChallenge
import CombineNetworking
import Combine

class ShiftsViewModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFetch1WeekShifts() throws {
		var disposables: Set<AnyCancellable> = []
		let viewModel = ShiftsViewModel()
		let expectation = expectation(description: "Fetch list of shifts for the upcoming week")
		
		viewModel.$shiftsData.dropFirst().sink { response in
			let distinctValues = Array(Set(response.map { $0.date }))
			let dayNumberFromMonday = Date().daysFromMonday
			
			if distinctValues.count == 7 - dayNumberFromMonday {
				expectation.fulfill()
			}
		}
		.store(in: &disposables)
		
		wait(for: [expectation], timeout: 7)
    }
	
	func testFetch3WeekShifts() throws {
		var disposables: Set<AnyCancellable> = []
		let viewModel = ShiftsViewModel()
		var fetchCount = 0
		let expectation = expectation(description: "Fetch list of shifts for the next 3 weeks")
		
		viewModel.$shiftsData.dropFirst().sink { response in
			if fetchCount == 2 {
				// Remove possible date duplicates to make sure exactly 3 weeks are loaded
				let distinctValues = Array(Set(response.map { $0.date }))
				let dayNumberFromMonday = Date().daysFromMonday
				
				if distinctValues.count == 21 - dayNumberFromMonday {
					expectation.fulfill()
				}
			} else {
				fetchCount += 1
				viewModel.fetchData()
			}
		}
		.store(in: &disposables)
		
		wait(for: [expectation], timeout: 21)
	}

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
