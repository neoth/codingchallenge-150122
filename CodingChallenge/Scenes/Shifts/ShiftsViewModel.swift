//
//  ShiftsViewModel.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import Foundation
import Combine
import CoreLocation

class ShiftsViewModel: ObservableObject, Identifiable, FetchableViewModel {
	@Published var shiftsData: [AvailableShiftsResponse.ShiftsData] = []
	@Published var coordinates: CLLocationCoordinate2D?
	
	let fetcher: ShiftsFetcher
	
	private var currentStartTime: Date?
	private var currentEndTime: Date?
	let searchLocation = "Dallas, TX"
	
	private var disposables: Set<AnyCancellable> = []
	
	init(fetcher: ShiftsFetcher = .init()) {
		self.fetcher = fetcher
		fetchData()
	}
	
	func fetchData() {
		updateStartAndEndTimes()
		executeFetch { [weak self] response in
			self?.shiftsData += response.data
			self?.coordinates = response.coordinates
		}
	}
	
	private func executeFetch(completion: @escaping (AvailableShiftsResponse) -> Void) {
		fetcher.fetchAvailableShifts(queryModel: prepareQueryModel())?
			.sink(receiveCompletion: {
				switch $0 {
				case .failure(let error):
					print(error.localizedDescription)
				default:
					return
				}
			}) { response in
				completion(response)
			}
			.store(in: &disposables)
	}
	
	private func updateStartAndEndTimes() {
		let newStartTime: Date
		if let currentEndTime = currentEndTime {
			newStartTime = Calendar.current.date(byAdding: .day, value: 1, to: currentEndTime) ?? Date()
		} else {
			newStartTime = Date()
		}
		currentStartTime = newStartTime
		// Here, as a value, I put 6, because API returns data in range <start, end>, not <start, end)
		currentEndTime = Calendar.current.date(byAdding: .day, value: 6 - newStartTime.daysFromMonday,
											   to: newStartTime)
	}
	
	private func prepareQueryModel() -> AvailableShiftsQueryModel {
		AvailableShiftsQueryModel(type: .list,
								  address: searchLocation,
								  start: currentStartTime?.dateString,
								  end: currentEndTime?.dateString,
								  radius: nil)
	}
}
