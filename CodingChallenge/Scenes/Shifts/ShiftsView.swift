//
//  ShiftsView.swift
//  CodingChallenge
//
//  Created by Brady Miller on 4/7/21.
//

import SwiftUI

struct ShiftsView: View {
	@ObservedObject var viewModel: ShiftsViewModel
	
	@State private var tappedItem: AvailableShiftsResponse.ShiftEntry? = nil
	
    var body: some View {
        NavigationView {
			List {
				if viewModel.shiftsData.count == 0 {
					CenteredProgressView()
				} else {
					ForEach(viewModel.shiftsData) { shiftData in
						if #available(iOS 15.0, *) {
							Section(shiftData.date) {
								ForEach(shiftData.shifts) { shift in
									ShiftSubView(model: shift)
										.onTapGesture {
											tappedItem = shift
										}
								}
							}
						} else {
							Text(shiftData.date)
							ForEach(shiftData.shifts) { shift in
								ShiftSubView(model: shift)
									.onTapGesture {
										tappedItem = shift
									}
							}
						}
					}
					CenteredProgressView()
						.onAppear {
							viewModel.fetchData()
						}
				}
			}
			.listStyle(.insetGrouped)
			.navigationTitle(ShiftKeys.title(city: viewModel.searchLocation).value)
			.sheet(item: $tappedItem, onDismiss: { tappedItem = nil }) { item in
				ShiftDetailsModalView(model: item)
			}
        }
    }
}

struct ShiftsView_Previews: PreviewProvider {
    static var previews: some View {
		ShiftsView(viewModel: .init())
    }
}
