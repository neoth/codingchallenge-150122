//
//  ShiftSubview.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import SwiftUI

struct ShiftSubView: View {
	let model: AvailableShiftsResponse.ShiftEntry
	
	var body: some View {
		VStack {
			Text(model.facilityType.name)
				.foregroundColor(Color(hex: model.facilityType.color))
				.padding(.bottom, 5)
				.font(.title3)
			
			HStack {
				Spacer()
				Text("\(ShiftKeys.covid.value): ")
				Text(model.covid ? GeneralKeys.yes.value : GeneralKeys.no.value)
				Spacer()
			}
			HStack {
				Spacer()
				Text("\(ShiftKeys.distance.value): ")
				Text(String(format: "%.0f mi", model.withinDistance.rounded()))
				Spacer()
			}
			.padding(.bottom, 5)
			
			TimeInfoView(startTime: model.normalizedStartDateTime,
						 endTime: model.normalizedEndDateTime,
						 timezone: model.timezone)
		}
		.font(.system(size: 15, weight: .medium, design: .default))
		.padding(.horizontal, 5)
		.padding(.vertical, 15)
	}
}

struct ShiftSubView_Previews: PreviewProvider {
	static var previews: some View {
		ShiftSubView(model: AvailableShiftsResponse.ShiftEntry.mock())
	}
}
