//
//  ShiftDetailsModalView.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import SwiftUI
import MapKit
import CoreLocation

struct ShiftDetailsModalView: View {
	let model: AvailableShiftsResponse.ShiftEntry
	
    var body: some View {
		VStack {
			Text(model.facilityType.name)
				.foregroundColor(Color(hex: model.facilityType.color))
				.padding(.bottom, 30)
				.font(.title)
			
			TimeInfoView(startTime: model.normalizedStartDateTime,
						 endTime: model.normalizedEndDateTime,
						 timezone: model.timezone)
				.padding(.bottom, 50)
			
			VStack {
				Group {
					FullWidthView(label: "\(ShiftKeys.shiftKind.value):",
								  value: model.shiftKind)
					
					FullWidthView(label: "\(ShiftKeys.covid.value):",
								  value: model.covid ? GeneralKeys.yes.value : GeneralKeys.no.value)
					
					FullWidthView(label: "\(ShiftKeys.premiumRate.value):",
								  value: model.premiumRate ? GeneralKeys.yes.value : GeneralKeys.no.value)
					
					FullWidthView(label: "\(ShiftKeys.distance.value):",
								  value: String(format: "%.0f mi", model.withinDistance.rounded()))
					
					FullWidthView(label: "\(ShiftKeys.skill.value):",
								  value: model.skill.name, valueColor: Color(hex: model.skill.color))
					
					FullWidthView(label: "\(ShiftKeys.speciality.value):",
								  value: model.localizedSpecialty.name,
								  valueColor: Color(hex: model.localizedSpecialty.specialty.color))
				}
				.padding(.bottom, 5)
			}
			
			Spacer()
		}
		.font(.system(size: 15, weight: .medium, design: .default))
		.padding(.horizontal, 30)
		.padding(.vertical, 30)
    }
}

struct ShiftDetailsModalView_Previews: PreviewProvider {
    static var previews: some View {
		ShiftDetailsModalView(model: AvailableShiftsResponse.ShiftEntry.mock())
    }
}
