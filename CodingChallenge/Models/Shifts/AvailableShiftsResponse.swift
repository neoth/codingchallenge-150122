//
//  AvailableShiftsResponse.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 14/01/2022.
//

import Foundation
import CoreLocation

struct AvailableShiftsResponse: Codable {
	let data: [ShiftsData]
	@CodableLocation var coordinates: CLLocationCoordinate2D?
	
	enum CodingKeys: String, CodingKey {
		case data
		case coordinates = "meta"
	}
}

extension AvailableShiftsResponse {
	struct ShiftsData: Codable, Identifiable {
		var id: String { date }
		
		let date: String
		let shifts: [ShiftEntry]
	}
	
	struct ShiftEntry: Codable, Identifiable {
		var id: Int { shiftId }
		
		let shiftId: Int
		let startTime: Date
		let endTime: Date
		let normalizedStartDateTime: String
		let normalizedEndDateTime: String
		let timezone: String
		let premiumRate: Bool
		let covid: Bool
		let shiftKind: String
		let withinDistance: Double
		let facilityType: FacilityType
		let skill: Skill
		let localizedSpecialty: LocalizedSpecialty
	}
	
	struct FacilityType: Codable {
		let id: Int
		let name: String
		let color: String
	}
	
	struct Skill: Codable {
		let id: Int
		let name: String
		let color: String
	}
	
	struct LocalizedSpecialty: Codable {
		let id: Int
		let specialtyId: Int
		let stateId: Int
		let name: String
		let abbreviation: String
		let specialty: Specialty
	}
	
	struct Specialty: Codable {
		let id: Int
		let name: String
		let color: String
		let abbreviation: String
	}
}

extension AvailableShiftsResponse.ShiftEntry {
	static func mock() -> Self {
		let specialty = AvailableShiftsResponse.Specialty(id: 0, name: "Spec", color: "#FFA123", abbreviation: "aa")
		
		let localizedSpecialty = AvailableShiftsResponse.LocalizedSpecialty(id: 0, specialtyId: 0, stateId: 0, name: "Specialty", abbreviation: "as", specialty: specialty)
		
		let skill = AvailableShiftsResponse.Skill(id: 0, name: "Skill", color: "#09ADAF")
		
		let facility = AvailableShiftsResponse.FacilityType(id: 0, name: "facility", color: "#123444")
		
		return .init(shiftId: 0, startTime: Date(), endTime: Date(), normalizedStartDateTime: "2022-01-01 10:00", normalizedEndDateTime: "2022-01-01 11:00", timezone: "central", premiumRate: false, covid: true, shiftKind: "kind", withinDistance: 10, facilityType: facility, skill: skill, localizedSpecialty: localizedSpecialty)
	}
}
