//
//  AvailableShiftsQueryModel.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 14/01/2022.
//

import Foundation

struct AvailableShiftsQueryModel: Queryable {
	let type: ResponseType
	let address: String
	let start: String?
	let end: String?
	let radius: Double?
}

extension AvailableShiftsQueryModel {
	enum ResponseType {
		case week, fourDay, list
	}
}
