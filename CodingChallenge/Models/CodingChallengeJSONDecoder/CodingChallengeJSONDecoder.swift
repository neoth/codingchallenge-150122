//
//  CodingChallengeJSONDecoder.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import Foundation

class CodingChallengeJSONDecoder: JSONDecoder {
	override init() {
		super.init()
		keyDecodingStrategy = .convertFromSnakeCase
		dateDecodingStrategy = .iso8601
	}
}
