//
//  LocalizationKeys.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 16/01/2022.
//

import Foundation

enum GeneralKeys: LocalizableKey {
	case yes, no
}

enum ShiftKeys: LocalizableKey {
	case shiftKind, covid, premiumRate, distance, skill, speciality, startTime, endTime, timezone
	case title(city: String)
	
	var value: String {
		switch self {
		case .title(let city):
			return generatedValue.replacingOccurrences(of: "{city}", with: city)
		default:
			return generatedValue
		}
	}
}
