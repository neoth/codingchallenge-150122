//
//  CenteredProgressView.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import SwiftUI

struct CenteredProgressView: View {
    var body: some View {
		HStack(alignment: .center, spacing: nil) {
			Spacer()
			ProgressView()
			Spacer()
		}
    }
}

struct CenteredProgressView_Previews: PreviewProvider {
    static var previews: some View {
        CenteredProgressView()
    }
}
