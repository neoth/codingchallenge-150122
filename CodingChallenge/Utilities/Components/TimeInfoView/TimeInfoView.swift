//
//  TimeInfoView.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import SwiftUI

struct TimeInfoView: View {
	let startTime: String
	let endTime: String
	let timezone: String
	
    var body: some View {
		HStack(alignment: .top) {
			TwoRowView(label: ShiftKeys.startTime.value,
					   value: startTime.replacingOccurrences(of: " ", with: "\n"))
			Spacer()
			TwoRowView(label: ShiftKeys.endTime.value,
					   value: endTime.replacingOccurrences(of: " ", with: "\n"))
			Spacer()
			TwoRowView(label: ShiftKeys.timezone.value,
					   value: timezone)
		}
    }
}

struct TimeInfoView_Previews: PreviewProvider {
    static var previews: some View {
        TimeInfoView(startTime: "2022-01-01 10:30", endTime: "2022-01-01 12:30", timezone: "Central")
    }
}
