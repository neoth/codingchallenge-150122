//
//  TwoRowView.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import SwiftUI

struct TwoRowView: View {
	let label: String
	let value: String
	var alignment: HorizontalAlignment = .center
	
    var body: some View {
		VStack(alignment: alignment, spacing: 5) {
			Text(label)
				.font(.headline)
			Text(value)
				.lineLimit(nil)
				.multilineTextAlignment(.center)
		}
    }
}

struct TwoRowView_Previews: PreviewProvider {
    static var previews: some View {
		TwoRowView(label: "Start time", value: "2022-01-01 10:00")
    }
}
