//
//  FullWidthView.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import SwiftUI

struct FullWidthView: View {
	let label: String
	let value: String
	var valueColor: Color? = nil
	
    var body: some View {
		HStack {
			Text(label)
				.font(.headline)
			Spacer()
			Text(value)
				.foregroundColor(valueColor)
		}
    }
}

struct FullWidthView_Previews: PreviewProvider {
    static var previews: some View {
		FullWidthView(label: "Label", value: "Value")
    }
}
