//
//  Date+daysFromMonday.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 16/01/2022.
//

import Foundation

extension Date {
	var daysFromMonday: Int {
		let dayNumber = Calendar.current.dateComponents([.weekday], from: self).weekday!
		let dayNumberFromMonday: Int
		
		switch dayNumber {
		case 1:
			dayNumberFromMonday = 6
		default:
			dayNumberFromMonday = dayNumber - 2
		}
		
		return dayNumberFromMonday
	}
}
