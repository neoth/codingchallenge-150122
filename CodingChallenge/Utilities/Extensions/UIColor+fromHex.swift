//
//  UIColor+fromHex.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import Foundation
import SwiftUI

extension Color {
	init(hex: String) {
		let code = hex.hasPrefix("#") ? String(hex.dropFirst()) : hex
		let mask = 0xFF
		var color: UInt64 = 0
		
		Scanner(string: code).scanHexInt64(&color)
		
		let red = Double(Int(color >> 16) & mask) / 255
		let green = Double(Int(color >> 8) & mask) / 255
		let blue = Double(Int(color) & mask) / 255
		
		self.init(UIColor(red: red, green: green, blue: blue, alpha: 1))
	}
}
