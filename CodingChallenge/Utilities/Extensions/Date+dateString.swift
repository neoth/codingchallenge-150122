//
//  Date+dateString.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import Foundation

extension Date {
	var dateString: String {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd"
		return formatter.string(from: self)
	}
}
