//
//  ShiftsEndpoint.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 14/01/2022.
//

import Foundation
import CombineNetworking

enum ShiftsEndpoint {
	case availableShifts(AvailableShiftsQueryModel)
}

extension ShiftsEndpoint: Endpoint {
	var baseURL: URL? {
		URL(string: "https://staging-app.shiftkey.com/")
	}
	
	var path: String {
		switch self {
		case .availableShifts:
			return "api/v2/available_shifts"
		}
	}
	
	var method: RequestMethod {
		.get
	}
	
	var headers: [String : Any]? {
		["Content-Type": "application/json"]
	}
	
	var data: EndpointData {
		switch self {
		case .availableShifts(let queryModel):
			return .queryParams(queryModel.toDictionary())
		}
	}
}
