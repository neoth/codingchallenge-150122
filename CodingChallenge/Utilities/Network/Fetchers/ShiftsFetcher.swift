//
//  ShiftsFetcher.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 14/01/2022.
//

import Foundation
import CombineNetworking
import Combine

class ShiftsFetcher: Fetchable {
	let provider: CNProvider<ShiftsEndpoint>
	let jsonDecoder: JSONDecoder
	
	init(provider: CNProvider<ShiftsEndpoint> = .init(), jsonDecoder: JSONDecoder = CodingChallengeJSONDecoder()) {
		self.provider = provider
		self.jsonDecoder = jsonDecoder
	}
	
	func fetchAvailableShifts(queryModel: AvailableShiftsQueryModel) -> AnyPublisher<AvailableShiftsResponse, Error>? {
		getPublisher(for: .availableShifts(queryModel), responseType: AvailableShiftsResponse.self)
	}
}
