//
//  FailableLocation.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import Foundation
import CoreLocation

@propertyWrapper
struct CodableLocation: Codable {
	var wrappedValue: CLLocationCoordinate2D?
	
	init(from decoder: Decoder) throws {
		guard let dictionary = (try? decoder.singleValueContainer().decode(Dictionary<String, Double>.self)),
			  let lat = dictionary["lat"], let long = dictionary["lng"] else { return }
		
		wrappedValue = CLLocationCoordinate2D(latitude: lat, longitude: long)
	}
	
	func encode(to encoder: Encoder) throws {
		let dictionary = ["lat": wrappedValue?.latitude, "lng": wrappedValue?.longitude]
		var container = encoder.singleValueContainer()
		try? container.encode(dictionary)
	}
}
