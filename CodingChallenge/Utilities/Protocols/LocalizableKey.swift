//
//  LocalizableKey.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 16/01/2022.
//

import Foundation

protocol LocalizableKey {
	var value: String { get }
	var generatedValue: String { get }
}

extension LocalizableKey {
	private var prefix: String { String(describing: type(of: self)) }
	private var suffix: String {
		var string = String(describing: self)
		if let firstIndex = string.firstIndex(of: "(") {
			string = String(string[...string.index(before: firstIndex)])
		}
		return string
	}
	
	var generatedValue: String {
		NSLocalizedString("\(prefix).\(suffix)", comment: "")
	}
	
	var value: String {
		generatedValue
	}
}
