//
//  Queryable.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 14/01/2022.
//

import Foundation

protocol Queryable {
	func toDictionary() -> [String: Any]
}

extension Queryable {
	func toDictionary() -> [String: Any] {
		let mirror = Mirror(reflecting: self)
		var output: [String: Any] = [:]
		
		mirror.children.forEach { child in
			
			/*
			 A workaround below this comment was made because child.value by default is of type Any, however in fact it can be optional, and because the output is what's then being sent to the backend, it's crucial to make sure the values in this dictionary are non-null and non-optional
			 */
			
			guard let label = child.label else { return }
			
			switch child.value {
			case Optional<Any>.none:
				return
			case Optional<Any>.some(let value):
				output[label] = value
			default:
				output[label] = child.value
			}
		}
		
		return output
	}
}
