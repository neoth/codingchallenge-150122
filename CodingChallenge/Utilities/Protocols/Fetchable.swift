//
//  Fetchable.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 14/01/2022.
//

import Foundation
import CombineNetworking
import Combine

protocol Fetchable {
	associatedtype T where T: Endpoint
	
	var provider: CNProvider<T> { get }
	var jsonDecoder: JSONDecoder { get }
	
	func getPublisher<U: Codable>(for endpoint: T, responseType: U.Type) -> AnyPublisher<U, Error>?
}

extension Fetchable {
	func getPublisher<U: Codable>(for endpoint: T, responseType: U.Type) -> AnyPublisher<U, Error>? {
		provider.publisher(for: endpoint, responseType: U.self, decoder: jsonDecoder)
	}
}
