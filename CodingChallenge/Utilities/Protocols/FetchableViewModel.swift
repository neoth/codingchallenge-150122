//
//  FetchableViewModel.swift
//  CodingChallenge
//
//  Created by Maciej Burdzicki on 15/01/2022.
//

import Foundation
import CombineNetworking

protocol FetchableViewModel {
	associatedtype T where T: Fetchable
	
	var fetcher: T { get }
}
